const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// const { dockStart } = require('@nlpjs/basic');

// // (async () => {
// //   await dockStart();
// //   console.log(dockStart);

  
// // })();

// let nlp = {};
// dockStart()
//   .then((dock) => {
//     nlp= dock.get('nlp');
//     console.log('Connected to NLP.js');
    
//   })
//   .catch((e) => {
//     console.log(e);
//     console.log('Unable to connect to NLP.js. Please check configuration.');
//   });


app.get('/', (req, res) => {
    res.send({'hello': 'Johnny'})
});


app.use(bodyParser.json());

app.post("/api/url", (req, res, next) => {
    res.json(["Syahmi","Syah"]);
   });

app.get("/url", (req, res, next) => {
    res.json(["test"]);
   });

const PORT = process.env.PORT || 5000;
app.listen(PORT);