import React, { Component } from 'react';
import axios from "axios"
// import { withRouter } from 'react-router-dom';

import Cookies from 'universal-cookie';
// import { v4 as uuid } from 'uuid';

// import Message from './Message';
import "./Chatbot.css";

const cookies = new Cookies();

class Chatbot extends Component {
    messagesEnd;
    talkInput;

    constructor(props) {
        super(props);
        this._handleInputKeyPress = this._handleInputKeyPress.bind(this);
        this.hide = this.hide.bind(this);
        this.show = this.show.bind(this);
        this.state = {
            messages: [],
            showBot: true,
        };
        // if (cookies.get('userID') === undefined) {
        //     cookies.set('userID', uuid(), { path: '/' });
        // }
    }

    async df_text_query (queryText) {
        let says = {
            speaks: 'user',
            msg: {
                text : {
                    text: queryText
                }
            }
        }
        this.setState({ messages: [...this.state.messages, says]});
        const res = await axios.post('/api/df_text_query',  {text: queryText, userID: cookies.get('userID')});
        for (let msg of res.data.fulfillmentMessages) {
            says = {
                speaks: '',
                msg: msg
            }
            this.setState({ messages: [...this.state.messages, says]});
        }
    };

    show(event) {
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot: true});
    }

    hide(event) {
        event.preventDefault();
        event.stopPropagation();
        this.setState({showBot: false});
    }

    
    _handleInputKeyPress(e) {
        if (e.key === 'Enter') {
            this.df_text_query(e.target.value);
            e.target.value = '';
        }
    }

    render() {
        if (this.state.showBot) {
            return (
                <div className="chatbot-window-select"> 
                    <div className="chatbot-box">
                        <div className="chatbot-box-header-wrapper">
                            <div className="chatbot-box-header">
                            <img src="/images/logo-j&t.png" alt="" width="42" height="42" style={{position: 'absolute'}}/>
                            <img src="/images/green-online-circle.png" alt="" width="8" height="8" style={{position: 'absolute', right: 269,bottom: 10}}/>
                                    <div className="chatbot-box-description">
                                        <span className="chatbot--bot--name">J&T Online Assistant</span>
                                        <span className="chatbot--bot--descp">Customer Support</span>
                                    </div>
                            <div className="chatbot-box-options">
                                <img src="/images/icons8-help-.png" alt="" width="20" height="20" style={{position: 'absolute', right: 52, bottom: 35}}/>
                                <a href="/" ><img src="/images/icons8-refresh.png" alt="" width="20" height="20" style={{position: 'absolute', right: 28, bottom: 35}}/></a>
                                <a href="/"  onClick={this.hide}><img src="/images/icons8-close-chat.png" alt="" width="20" height="20" style={{position: 'absolute', right: 3, bottom: 35}} ></img></a>
                                <a href ="https://www.wipdata.com/" style={{position: 'absolute', right: 3, bottom: 0, color: 'white', fontFamily: 'sans-serif', fontSize: '5px'}}>POWERED BY WIPDATA</a>
                            </div>   
                            </div>
                            
                            </div>
                            <div className="test-messages-box"> 
                                {/* {this.renderMessages(this.state.messages)}  */}
                       
                                <div ref={(el) => { this.messagesEnd = el; }}
                                        style={{ float:"right"}}>
                                {/* , clear: "both"  */}
                            </div>   
                        </div>
                        <div className="message-outer-input-box">
                            <div className="message-input-box" >
                                <input className="message-placeholder-input" type="text" placeholder="Type a message" onKeyPress={this._handleInputKeyPress} id="user_says" type="text"></input>
                            </div>
                        </div>
                        
                    </div>
                </div>
            );
        } else {
            return (
                <div className="chatbot-chat-icon" id="open-chatbot-box">
                        <div>
                            <ul>
                                <a href="/" onClick={this.show}><img src="/images/logo-j&t.png" alt="" width="40" height="40" /></a>
                            </ul>
                        </div>
                    
                            <div ref={(el) => { this.messagesEnd = el; }}
                                style={{ float:"left", clear: "both" }}>
                            </div>
                </div>
            );
        }
    }
}
//withRouter
export default Chatbot;