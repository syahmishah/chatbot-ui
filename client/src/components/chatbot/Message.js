import React from 'react';
import "./Chatbot.css";

const Message = (props) => {
    return (

        <div className="bot--messages--box">Hello
            <div className="bot-messages1-box2">
                <div>
                    {props.speaks==='bot' &&
                    <div >
                        <a href="/" className="btn-floating btn-large waves-effect waves-light red">{props.speaks}</a>
                    </div>
                    }
                    <div>
                      <span className="black-text">
                        {props.text}
                      </span>
                    </div>
                    {props.speaks==='user' &&
                    <div>
                        <a href="/" className="btn-floating btn-large waves-effect waves-light red">{props.speaks}</a>
                    </div>
                    }
                </div>
            </div>
        </div>

    );
};

export default Message;
